const { app, BrowserWindow } = require('electron');
const path = require('path');
const url = require('url');
const ipcMain = require('electron').ipcMain;

let win;

function createWindow() {
   // Create the browser window.
   win = new BrowserWindow({ frame: true })

   //win.setMenu(null);

   // and load the index.html of the app.
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/index.html'),
      protocol: 'file:',
      slashes: true
   }));

   // Emitted when the window is closed.
   win.on('closed', () => {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      win = null;
   });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
   // On macOS it is common for applications and their menu bar
   // to stay active until the user quits explicitly with Cmd + Q
   if (process.platform !== 'darwin') {
      app.quit()
   }
});

app.on('activate', () => {
   // On macOS it's common to re-create a window in the app when the
   // dock icon is clicked and there are no other windows open.
   if (win === null) {
      createWindow();
   }
});

//event listener dari instance process
ipcMain.on('open-idc', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/idc.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('open-eec', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/eec.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('open-rescue', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/rescue.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('open-sumo', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/sumo.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('open-soccer', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/soccer.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('open-timer-tc', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/tc.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('open-eec-semifinal', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/eec-semifinal.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('back-home', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/index.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('open-eec-praktikum', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/eec-praktikum.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('open-eec-trial', function(event, arg){
   win.loadURL(url.format({
      pathname: path.join(__dirname, '/pages/eec-trial.html'),
      protocol: 'file:',
      slashes: true
   }));
});

ipcMain.on('logger', function(event, arg){
   console.log(arg);
});