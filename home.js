const ipcRenderer = require('electron').ipcRenderer;

var win;

var idcCard = document.getElementById('idc-card');
var eecCard = document.getElementById('eec-card');
var rescueCard = document.getElementById('rescue-card');
var sumoCard = document.getElementById('sumo-card');
var soccerCard = document.getElementById('soccer-card');
var tcImg = document.getElementById('timer-tc');

idcCard.addEventListener('click', function (element, event) {
   ipcRenderer.send('open-idc', '');
});

eecCard.addEventListener('click', function(element, event){
   ipcRenderer.send('open-eec-trial', '');
});

rescueCard.addEventListener('click', function(element, event){
   ipcRenderer.send('open-rescue', '');
});

sumoCard.addEventListener('click', function(element, event){
   ipcRenderer.send('open-sumo', '');
});

soccerCard.addEventListener('click', function(element, event){
   ipcRenderer.send('open-soccer', '');
});

tcImg.addEventListener('click', function(element, event){
   ipcRenderer.send('open-timer-tc', '');
});