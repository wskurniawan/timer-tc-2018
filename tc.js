const ipcRenderer = require('electron').ipcRenderer;
const moment = require('moment');
const Player = require('howler').Howl;

//inisiasi komponen
var timerHolder = document.getElementById('timer-holder');
var btnHome = document.getElementById('btn-home');
var btnStart = document.getElementById('btn-start');
var btnPause = document.getElementById('btn-pause');
var btnRestart = document.getElementById('btn-restart');
var btnResetYes = document.getElementById('btn-reset-yes');
var modalReset = document.getElementById('modal-reset');
var modalSetTimer = document.getElementById('modal-set-timer');
var btnSetTImer = document.getElementById('btn-set-timer');
var btnConfirmSetTimer = document.getElementById('btn-set-yes')

//load sound alert
var soundAlert = new Player({
   src: [ __dirname + '/assets/sound/notif.mp3']
});

var timesUpAlert = new Player({
   src: [ __dirname + '/assets/sound/times-up.mp3']
});

timerHolder.innerText = '1 hari : 12 jam : 03 menit : 24 detik';

var timestampEnd = 1520762400000;

//waktu timer
var time = timestampEnd - Date.now();

//state timer saat ini
var timerNow;

btnHome.addEventListener('click', function(element, event){
   ipcRenderer.send('back-home', '');
});

btnStart.addEventListener('click', function(element, event){
   if(time === 0){
      //set ulang color timer
      resetTimerColor();

      resetTime();
   }

   time = timestampEnd - Date.now();

   btnStart.style.display = 'none';
   btnPause.style.display = 'inline';
   timerNow = setInterval(function(){
      time =  time - 1000;
      var strTime = '';
      
      //sesuaikan warna timer
      adjustTImerColor();

      //sound player
      if(moment.duration(time).asSeconds() === 60 || moment.duration(time).asSeconds() === 30 || moment.duration(time).asSeconds() === 10 || (moment.duration(time).asSeconds() <= 5 && moment.duration(time).asSeconds() > 0)){
         soundAlert.play();
      }

      strTime = strTime + moment.duration(time).days() + ' hari : ';

      if(moment.duration(time).hours() < 10){
         strTime = strTime + '0';
      }
      strTime = strTime + moment.duration(time).hours() + ' jam : ';

      if(moment.duration(time).minutes() < 10){
         strTime = strTime + '0';
      }
      strTime = strTime + moment.duration(time).minutes() + ' menit : ';

      if(moment.duration(time).seconds() < 10){
         strTime = strTime + '0';
      }
      strTime = strTime + moment.duration(time).seconds() + ' detik';

      timerHolder.innerText = strTime;

      if(time === 0){
         //timesUpAlert.play();

         clearInterval(timerNow);
         btnPause.style.display = 'none';
         btnStart.style.display = 'inline';
      }   
   }, 1000);
});

btnPause.addEventListener('click', function(element, event){
   btnPause.style.display = 'none';
   btnStart.style.display = 'inline';

   clearInterval(timerNow);
});

btnRestart.addEventListener('click', function(element, event){
   clearInterval(timerNow);

   if(time != 0){
      clearInterval(timerNow);
   }

   if(btnStart.style.display === 'none'){
      btnPause.style.display = 'none';
      btnStart.style.display = 'inline';
   }

   if(time != 0){
      return UIkit.modal(modalReset).show();
   }
   
   //set ulang color timer
   resetTimerColor();

   resetTime();
});

btnResetYes.addEventListener('click', function(element, event){
   //set ulang color timer
   resetTimerColor();

   resetTime();
});

//untuk reset waktu
function resetTime(){
   time = timestampEnd - Date.now();
   timerHolder.innerText = '0 hari : 00 jam : 00 menit : 00 detik';
}

btnSetTImer.addEventListener('click', function(element, event){
   clearInterval(timerNow);

   if(time != 0){
      clearInterval(timerNow);
   }

   if(btnStart.style.display === 'none'){
      btnPause.style.display = 'none';
      btnStart.style.display = 'inline';
   }

   UIkit.modal(modalSetTimer).show();
});

btnConfirmSetTimer.addEventListener('click', function(element, event){
   var strJam = document.getElementById('set-end-jam').value;
   var strMenit = document.getElementById('set-end-menit').value;

   var strTgl = document.getElementById('set-end-tgl').value;
   var strBulan = document.getElementById('set-end-bulan').value;
   var strTahun = document.getElementById('set-end-tahun').value;

   var strWaktu = strTahun + '-' + strBulan + '-' + strTgl + ' ' + strJam + ':' + strMenit;

   ipcRenderer.send('logger', strWaktu);

   timestampEnd = new Date(parseInt(strTahun), parseInt(strBulan), parseInt(strTgl), parseInt(strJam), parseInt(strMenit), 0, 0).getTime();

   ipcRenderer.send('logger', timestampEnd);

   time = timestampEnd - Date.now();

   var strTime = '';

   strTime = strTime + moment.duration(time).days() + ' hari : ';

      if(moment.duration(time).hours() < 10){
         strTime = strTime + '0';
      }
      strTime = strTime + moment.duration(time).hours() + ' jam : ';

      if(moment.duration(time).minutes() < 10){
         strTime = strTime + '0';
      }
      strTime = strTime + moment.duration(time).minutes() + ' menit : ';

      if(moment.duration(time).seconds() < 10){
         strTime = strTime + '0';
      }
      strTime = strTime + moment.duration(time).seconds() + ' detik';

      timerHolder.innerText = strTime;

   //set ulang color timer
   adjustTImerColor();
});

//untuk menyesuaikan warna timer
function adjustTImerColor(){
   if(moment.duration(time).asSeconds() > 60){
      timerHolder.style.color = 'inherit';
   }

   if(moment.duration(time).asSeconds() <= 60 && moment.duration(time).asSeconds() > 30){
      timerHolder.style.color = '#D84315';
   }

   if(moment.duration(time).asSeconds() <= 30 && moment.duration(time).asSeconds() > 10){
      timerHolder.style.color = '#C62828';
   }

   if(moment.duration(time).asSeconds() <= 10 && moment.duration(time).asSeconds() > 5){
      timerHolder.style.color = '#D32F2F';
   }

   if(moment.duration(time).asSeconds() <= 5){
      timerHolder.style.color = '#E53935';
   }
}

//reset timer color
function resetTimerColor(){
   timerHolder.style.color = 'inherit';
}

document.addEventListener('readystatechange', event => {
   if (event.target.readyState === "complete") {
      if(time === 0){
         //set ulang color timer
         resetTimerColor();
   
         resetTime();
      }
   
      btnStart.style.display = 'none';
      btnPause.style.display = 'inline';
      timerNow = setInterval(function(){
         time =  time - 1000;
         var strTime = '';
         
         //sesuaikan warna timer
         adjustTImerColor();
   
         //sound player
         if(moment.duration(time).asSeconds() === 60 || moment.duration(time).asSeconds() === 30 || moment.duration(time).asSeconds() === 10 || (moment.duration(time).asSeconds() <= 5 && moment.duration(time).asSeconds() > 0)){
            soundAlert.play();
         }
   
         strTime = strTime + moment.duration(time).days() + ' hari : ';
   
         if(moment.duration(time).hours() < 10){
            strTime = strTime + '0';
         }
         strTime = strTime + moment.duration(time).hours() + ' jam : ';
   
         if(moment.duration(time).minutes() < 10){
            strTime = strTime + '0';
         }
         strTime = strTime + moment.duration(time).minutes() + ' menit : ';
   
         if(moment.duration(time).seconds() < 10){
            strTime = strTime + '0';
         }
         strTime = strTime + moment.duration(time).seconds() + ' detik';
   
         timerHolder.innerText = strTime;
   
         if(time === 0){
            timesUpAlert.play();
   
            clearInterval(timerNow);
            btnPause.style.display = 'none';
            btnStart.style.display = 'inline';
         }   
      }, 1000);
   }
 });